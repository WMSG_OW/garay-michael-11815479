#pragma once
#include<string>

using namespace std;

class Pokemons {
public:
	Pokemons();
	Pokemons(int baseHP, int hp, int baseDamage, int xp, int maxXp, int level);
	int baseHP;
	int hp;
	string wildPokemon[15]{ "Bulbasaur","Charmander","Squirtle","Magikarp","Ratata","Pidgey" ,"Weedle", "Caterpie", "Eevee", "Weedle","Vulpix","Pikachu","Spearow","Ekans","Jigglypuff" };
	int baseDamage; 
	int xp;
	int maxXp;
	int level;
	int xpDrop;
	int pokemonInventorySize = 1;
	int starterLevel = 5;
	int encounter;
	int capture; 
	int randDamage = rand() % 20 + 10;
	int randxP = rand() % 20 + 1;
	int randlvl = rand() % 10 + 1;
	int enc = rand() % 15 + 1;
	int cap = rand() % 10 + 1;
	string pokemonInventory[6];
	string starters[3]{ "Bulbasaur", "Charmander", "Squirtle" };

	void stats(int baseHP, int hp, int baseDamage, int xpDrop, int maxXp, int level, int inventorySize, int starterLevel);

	void battle(int baseHP, int baseDamage, int encounter,  int xp, int maxXp, int pokemonInventorySize);

	void encounterPokemons(int encounter, int level, int baseHP, int baseDamage, int xp, int maxXp);

	void capturePokemon(int capture, int pokemonInventorySize, int encounter);

	void starter(int starterChoice);
		

};